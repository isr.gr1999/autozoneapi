<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ProveedorController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\PedidoController;
use App\Models\Categoria;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

*/

Route::post('auth/register',[AuthController::class,'create']);
Route::post('auth/login',[AuthController::class,'login']);

Route::middleware(['auth:sanctum'])->group(function(){

Route::resource('categorias', CategoriaController::class);
Route::resource('proveedores', ProveedorController::class);
Route::resource('productos', ProductoController::class);
Route::resource('pedidos', PedidoController::class);


Route::get('productobycategoria',[ProductoController::class,'ProductoByCategoria']);
Route::get('productobyproveedor',[ProductoController::class,'ProductoByProveedor']);
Route::get('productosall',[ProductoController::class,'all']);


Route::get('pedidobyproducto',[PedidoController::class,'PedidoByProducto']);
Route::get('pedidosall',[PedidoController::class,'all']);


Route::get('auth/logout',[AuthController::class,'logout']);
});




