<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use Illuminate\Http\Request;
use App\Models\Producto;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PedidoController extends Controller
{
    
    public function index()
    {
        $pedido = Pedido::select('pedidos.*','productos.nomProducto as producto')->join('productos','pedidos.producto_id','=','productos.id')->paginate(10);
        return response()->json($pedido);
    }

    
    public function store(Request $request)
    {
        $rules = [
            'nomCliente' => 'required|string|min:1|max:100',
            'direccion' => 'required|string|min:1|max:100',
            'telefono' => 'required|max:15',
            'producto_id'  => 'required|numeric'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $pedido = new Pedido($request->input());
        $pedido->save();
        return response()->json([
            'status' => true,
            'message' => 'Pedido creado'
        ],200);
    }

    
    public function show(Pedido $pedido)
    {
        return response()->json([
            'status' => true,
            'data' => $pedido
        ],200);
    }

    
    public function update(Request $request, Pedido $pedido)
    {
        $rules = [
            'nomCliente' => 'required|string|min:1|max:100',
            'direccion' => 'required|string|min:1|max:100',
            'telefono' => 'required|max:50',
            'producto_id'  => 'required|numeric'
        ];
        $validator = Validator::make($request->input(),$rules);
        if($validator->fails()){
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()->all()
            ],400);
        }
        $pedido->update($request->input());
        return response()->json([
            'status' => true,
            'message' => 'Pedido actualizado'
        ],200);
    }

    
    public function destroy(Pedido $pedido)
    {
        $pedido->delete();
        return response()->json([
            'status' => true,
            'message' => 'Pedido elimiando'
        ],200);
    }

    public function PedidoByProducto(){
        $pedido = Pedido::select(DB::raw('count(pedidos.id) as count', 'productos.nomCliente'))->join('productos','productos.id' ,'=', 'pedidos.producto_id')->groupBy('productos.nomProducto')->get();
    }

    public function all(){
        $pedido = Pedido::select('pedidos.*','productos.nomProducto as producto')->join('productos','pedidos.producto_id','=','productos.id');
        return response()->json($pedido);
    }
}
