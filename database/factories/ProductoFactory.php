<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Producto>
 */
class ProductoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'nomProducto' => $this->faker->word,
            'categoria_id' => $this->faker->numberBetween(1,6),
            'proveedor_id' => $this->faker->numberBetween(1,6),
            'precio' => $this->faker->randomNumber($nbDigits = 5, $strict = false)
        ];
    }
}
